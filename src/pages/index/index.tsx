import { View, Text, Slider, Textarea } from "@tarojs/components";

import "./index.scss";

export default function Index() {
  return (
    <View
      className="index"
      onTouchMove={(e) => {
        console.log("e ---> ", e);
      }}
    >
      <Textarea className="textarea" placeholder="1111" autoHeight />
    </View>
  );
}
