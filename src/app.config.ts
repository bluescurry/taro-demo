export default defineAppConfig({
  tabBar: {
    color: "#555",
    selectedColor: "#222",
    backgroundColor: "#FFF",
    list: [
      {
        pagePath: "pages/index/index",
        text: "首页",
      },
      {
        pagePath: "pages/order/index",
        text: "订单",
      },
      {
        pagePath: "pages/center/index",
        text: "我的",
      },
    ],
  },
  pages: ["pages/index/index", "pages/center/index"],
  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#fff",
    navigationBarTitleText: "WeChat",
    navigationBarTextStyle: "black",
  },
  animation: false,
});
